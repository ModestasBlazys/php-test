<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Paginator;

final class PaginatorTest extends TestCase
{
    public function testReturnStructure() : void
    {
        $data = array(
            'item1', 'item2', 'item3'
        );
        
        $paginator = new Paginator($data);

        $results = $paginator->paginate(1, 2);
        
        $this->assertArrayHasKey('data', $results);
        $this->assertArrayHasKey('current_page', $results);
        $this->assertArrayHasKey('per_page', $results);
        $this->assertArrayHasKey('total', $results);
    }

    public function testIfPaginates() : void
    {
        $data = array(
            'item1', 'item2', 'item3'
        );
        
        $paginator = new Paginator($data);

        $results = $paginator->paginate(1, 2);
        
        $this->assertCount(2, $results['data']);
        $this->assertArraySubset(['data' => ['item1', 'item2']], $results);

        $results = $paginator->paginate(2, 2);

        $this->assertCount(1, $results['data']);
        $this->assertArraySubset(['data' => ['item3']], $results);

        $results = $paginator->paginate(3, 2);

        $this->assertCount(0, $results['data']);
    }

    public function testDefaultPage() : void
    {
        $data = array(
            'item1', 'item2', 'item3'
        );
        
        $paginator = new Paginator($data);

        $results = $paginator->paginate(-1, 2);
        
        $this->assertCount(2, $results['data']);
    }
}