<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Cache;

final class CacheTest extends TestCase
{
    public function testCreateInstance() : void
    {
        $cache = new Cache;
        
        $this->assertInstanceOf(
            Cache::class,
            $cache
        );
    }

    public function testReturnPool() : void
    {
        $cache = new Cache;

        $this->assertInstanceOf(
            \Cache\Adapter\Filesystem\FilesystemCachePool::class,
            $cache->getPool()
        );
    }
}