<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Cache;
use App\Api;
use App\Services\ListPokemonsService;

final class ListPokemonsServiceTest extends TestCase
{
    public function testRetrievePokemons() : void
    {
        $service = new ListPokemonsService;
        
        $results = $service->handle(null, 1, 16);

        $this->assertArrayHasKey('data', $results);
        $this->assertArrayHasKey('per_page', $results);
        $this->assertArrayHasKey('current_page', $results);
        $this->assertArrayHasKey('total', $results);
        
        $this->assertCount(16, $results['data']);
        $this->assertArraySubset(['per_page' => 16], $results);
        $this->assertArraySubset(['current_page' => 1], $results);

        $results = $service->handle(null, 2, 10);

        $this->assertCount(10, $results['data']);
        $this->assertArraySubset(['per_page' => 10], $results);
        $this->assertArraySubset(['current_page' => 2], $results);
    }

    public function testRetrieveFilteredPokemons() : void
    {
        $service = new ListPokemonsService;
        
        $results = $service->handle('bulbasaur', 1, 16);

        $this->assertCount(1, $results['data']);
        $this->assertEquals('bulbasaur', $results['data'][0]->name);
    }
}