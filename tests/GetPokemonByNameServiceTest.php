<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Cache;
use App\Api;
use App\Services\GetPokemonByNameService;

final class GetPokemonByNameServiceTest extends TestCase
{
    public function testRetrievePokemon() : void
    {
        $service = new GetPokemonByNameService;
        
        $results = $service->handle('bulbasaur');

        $this->assertArrayHasKey('data', $results);
    }

}