<?php

require __DIR__.'/../vendor/autoload.php';

use App\Exceptions\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

function exception_handler($exception) {

    $message = 'Shame on us! Something went wrong';
    $code = 500;

    if ($exception instanceof ResourceNotFoundException) {
        $message = 'Resource not found';
        $code = 404;
    }

    $request = Request::createFromGlobals();
    
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $response = new JsonResponse(['data' => $message], $code);
    } else {
        $response = new Response($message, $code);
    }
    
    $response->send();
}

set_exception_handler('exception_handler');