<?php

namespace App\Services;

use App\Api;
use App\Cache;

class GetPokemonByNameService
{
    private $cache;
    private $api;

    /**
     * Set dependencies
     */
    public function __construct()
    {
        $this->cache = (new Cache)->getPool();
        $this->api = new Api;
    }
    
    /**
     * Retrieve pokemon by name
     *
     * @param mixed $name
     * @return array
     */
    public function handle($name) : array
    {
        $key = 'pokemon_' . md5($name);
        // get pokemon from cache
        $pokemon = $this->cache->get($key);

        // if pokemon is not cached, retrieve it from pokemon api and cache it
        if (!$pokemon) {
            $this->cache->set($key, $this->api->getPokemonByName($name));
            $pokemon = $this->cache->get($key);
        }

        return [
            'data' => $pokemon
        ];
    }
}