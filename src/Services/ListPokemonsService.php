<?php

namespace App\Services;

use App\Api;
use App\Cache;
use App\Paginator;

class ListPokemonsService
{
    private $cache;
    private $api;

    /**
     * Set dependencies
     */
    public function __construct()
    {
        $this->cache = (new Cache)->getPool();
        $this->api = new Api;
    }
    
    /**
     * Retrieves paginated list of pokemons filtered by name
     *
     * @param mixed $name
     * @param integer $page
     * @param integer $perPage
     * @return array
     */
    public function handle($name, int $page, int $perPage = 16) : array
    {
        $key = 'pokemons';
        // get pokemons from cache
        $pokemons = $this->cache->get($key);

        // if pokemons are not cached, retrieve it from pokemon api and cache them
        if (!$pokemons) {
            $this->cache->set($key, $this->api->getPokemons());
            $pokemons = $this->cache->get($key);
        }

        // filter pokemons by name
        foreach ($pokemons->results as $key => $pokemon) {
            if (!is_null($name) && !empty($name) && strpos($pokemon->name, $name) === false) {
                unset($pokemons->results[$key]);
            }
        }

        return (new Paginator($pokemons->results))->paginate($page, $perPage);
    }
}