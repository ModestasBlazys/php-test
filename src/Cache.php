<?php

namespace App;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use Cache\Adapter\Filesystem\FilesystemCachePool;

class Cache
{
    private $pool;

    /**
     * Set dependencies
     */
    public function __construct()
    {
        $filesystemAdapter = new Local(__DIR__.'/');
        $filesystem = new Filesystem($filesystemAdapter);
        $this->pool = new FilesystemCachePool($filesystem);
    }

    /**
     * Undocumented function
     *
     * @return object
     */
    public function getPool() : object
    {
        return $this->pool;
    }
}