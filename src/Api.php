<?php

namespace App;

use Exception;
use GuzzleHttp\Client;
use App\Exceptions\ResourceNotFoundException;

class Api
{
    private $url = 'https://pokeapi.co/api/v2/';
    private $client;
    
    /**
     * Set client
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->url,
            'verify' => false
        ]);
    }
    
    /**
     * Get pokemons
     *
     * @return object
     */
    public function getPokemons() : object
    {
        return $this->getResponse('pokemon/');
    }

    /**
     * Get pokemon by name
     *
     * @param string $name
     * @return object
     */
    public function getPokemonByName(string $name) : object
    {
        return $this->getResponse('pokemon/' . $name . '/');
    }

    /**
     * Get response
     *
     * @param string $resource
     * @return object
     */
    protected function getResponse(string $resource) : object
    {
        try {
        
            $response = $this->client->request('GET', $resource);

        } catch (Exception $e) {

            throw new ResourceNotFoundException;

        }
        
        $body = $response->getBody();

        return json_decode($body, true);
    }
}