<?php

namespace App;

class Paginator
{
    private $data;
    private $perPage;
    private $page;

    /**
     * Undocumented function
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Paginate the data
     *
     * @param integer $page
     * @param integer $perPage
     * @return array
     */
    public function paginate(int $page = 1, int $perPage = 15) : array
    {
        $page = $page < 0 ? 1 : $page;
        
        $total = $this->getTotal();
        $offset = $perPage * ($page - 1);
        $results = array_slice($this->data, $offset, $perPage);

        return [
            'data' => $results,
            'current_page' => $page,
            'per_page' => $perPage,
            'total' => $total
        ];
    }

    /**
     * Get total data count
     *
     * @return integer
     */
    private function getTotal() : int
    {
        return count($this->data);
    }
}