import Axios from 'axios';
import Vue from 'vue';
import VueRouter from 'vue-router';
import PokemonsPage from './pages/PokemonsPage.vue';

Vue.use(VueRouter);
Vue.prototype.$axios = Axios;

const router = new VueRouter({
    routes: [
        { 
            name: 'pokemons',
            path: '/', 
            component: PokemonsPage 
        }
    ]
});

const app = new Vue({
    el: '#app',
    router
});

