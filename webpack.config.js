'use strict'

const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
  mode: 'production',
  entry: [
    './src/assets/js/app.js'
  ],
  output: {
    path: __dirname + "/public/js",
    filename: 'app.js'
  },
  resolve: {
    alias: {
        'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin()
  ]
}