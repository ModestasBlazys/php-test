<?php

/**
 * This endpoint retrieves pokemon details by pokemon name
 */

require __DIR__.'/../../src/bootstrap.php';

use App\Services\GetPokemonByNameService;
use Symfony\Component\HttpFoundation\JsonResponse;

$getPokemonByNameService = new GetPokemonByNameService;

$name = $_GET['name'] ?? null;

$pokemon = $getPokemonByNameService->handle($name);

$response = new JsonResponse($pokemon);

$response->send();