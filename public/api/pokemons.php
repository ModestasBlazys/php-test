<?php

/**
 * This endpoint retrieves paginated list of pokemons. Use "page" query string to navigate.
 * Pokemons can be filtered using "name" query string and "page" to
 */

require __DIR__.'/../../src/bootstrap.php';

use App\Services\ListPokemonsService;
use Symfony\Component\HttpFoundation\JsonResponse;

$listPokemonService = new ListPokemonsService;

$name = $_GET['name'] ?? null;
$page = $_GET['page'] ?? 1;

$results = $listPokemonService->handle($name, $page, 16);

$response = new JsonResponse($results);

$response->send();