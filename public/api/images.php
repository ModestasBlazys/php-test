
<?php

/**
 * This endpoint retrieves image url which is used to serve images from VueJs
 */

require __DIR__.'/../../src/bootstrap.php';

use Symfony\Component\HttpFoundation\JsonResponse;

$_SERVER['REQUEST_SCHEME'] = $_SERVER['REQUEST_SCHEME'] ?? 'https';

// this ensures that any incoming image requests are domain specific
$domain = 'https://raw.githubusercontent.com';

$url = $_GET['url'] ?? null;

// if image cannot be located in local folder we request it from remote repository
// and store them localy so next time it can be served from cache
if (empty($url) || substr($url, 0, strlen($domain)) !== $domain) {
    $response = new JsonResponse(['data' => 'file not found'], 404);
} else {
    $cache = new ImageCache\ImageCache;
    $cache->cached_image_directory = __DIR__ . '/../images/cache';
    $response = new JsonResponse(['data' => $cache->cache($url)], 200);
}

// here we return local image url and not the actual image
$response->send();